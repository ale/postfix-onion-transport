postfix-onion-transport
=======================

A Postfix dynamic map server prototype, using the
[tcp_table(5)](http://www.postfix.org/tcp_table.5.html) protocol, that
can lookup SRV records in DNS and generate transport directives with
the result. This is primarily used to route traffic over the Tor
network using a dedicated, Tor-aware transport, and using SRV records
in place of MX for relay discovery.

For example, the presence of the following SRV record:

    _onion-mx._tcp.example.com. IN SRV 10 5 25 ysp4gfuhnmj6b4mb.onion.

will cause a lookup on the postfix-onion-transport map for
`example.com` to return the following:

    smtptor:[ysp4gfuhnmj6b4mb.onion:25]

When used as a [transport(5)](http://www.postfix.org/transport.5.html)
map, this will tell Postfix to route the message to the given Tor
Hidden Service using the *smtptor* transport (which is expected to be
a Tor-aware smtp client that you have set up).

# Installation

To build p-o-t, you'll need a relatively recent Go environment (at
least version 1.5). You can download and build the code with a single
`go get` command as follows:

    go get -tags netgo git.autistici.org/ale/postfix-onion-transport

Alternatively, if you have already a copy of this repository in the
right place in your $GOPATH, you can build the daemon simply with:

    go build -tags netgo ./

which will create the `postfix-onion-transport` binary in the current
directory.

The `netgo` build tag is critical: if it's missing there is a chance
that the final binary might use the C library resolver instead of the
native Go one, resulting in insufficient performance. For more details
see the
[Go standard library documentation](https://golang.org/pkg/net). There's
an easy way to check: binaries built with `-tags netgo` will be fully
static.

There is a systemd unit file in
`contrib/postfix-onion-transport.service`. It expects a user named
`postfix-onion-transport`, and the binary in
`/usr/sbin/postfix-onion-transport`.

# Usage

The `postfix-onion-transport` daemon listens on port 6013 by default
but this can be controlled with the `--addr` option, i.e.:

    postfix-onion-transport --addr=127.0.0.1:2345

The daemon will generate responses using the `smtptor` service by
default. To return a different transport for successful lookups, use
the `--transport` option.

The service used in SRV lookups (default `onion-mx`) can be changed
with the `--service` option.

On the Postfix side, you can talk to the daemon using a *tcp_table*
entry like this:

    transport_maps = tcp:127.0.0.1:2345

## Blacklisting domains

It is possible to blacklist specific domains: in this case, the daemon
will never perform a SRV query for them, and it will always return a
"not found" result. You can list domains on the command line with the
`--exclude-domains` option, or load them from a file with
`--exclude-domains-file`.

## Handling of errors

DNS errors, for instance resolver failures as opposed to successful
NXDOMAIN responses, will result in a temporary lookup error. This
should cause Postfix to enqueue the message and retry the lookup
later.

