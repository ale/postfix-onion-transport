package main

import (
	"bufio"
	"flag"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"git.autistici.org/ale/postfix-onion-transport/server"
)

var (
	addr             = flag.String("addr", ":6013", "Address to listen on. This can specify either a TCP address in [HOST]:PORT format, or a UNIX socket path")
	debug            = flag.Bool("debug", false, "Enable debug logging")
	srvService       = flag.String("service", "onion-mx", "Service for SRV lookup")
	postfixTransport = flag.String("transport", "smtptor", "Target Postfix transport")
	maxInflight      = flag.Int("max-inflight-requests", 1000, "Maximum number of inflight requests")
	blacklistDomains = flag.String("exclude-domains", "", "Exclude these domains from lookups (comma-separated list)")
	blacklistFile    = flag.String("exclude-domains-file", "", "Exclude domains listed in this file, one per line")
)

type blacklist map[string]struct{}

func (b blacklist) add(domain string) {
	b[domain] = struct{}{}
}

func (b blacklist) addFile(path string) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}
		b.add(line)
	}
	return scanner.Err()
}

func (b blacklist) addToMap(bl *pot.BlacklistMap) {
	for domain := range b {
		bl.Add(domain)
	}
}

func loadBlacklist(pbl *pot.BlacklistMap) {
	bl := make(blacklist)
	bl.add("localhost")
	bl.add("localhost.localdomain")
	if *blacklistDomains != "" {
		for _, d := range strings.Split(*blacklistDomains, ",") {
			bl.add(strings.TrimSpace(d))
		}
	}
	if *blacklistFile != "" {
		if err := bl.addFile(*blacklistFile); err != nil {
			log.Fatal(err)
		}
	}
	bl.addToMap(pbl)
}

func createListener() (net.Listener, error) {
	if strings.Contains(*addr, ":") {
		return net.Listen("tcp", *addr)
	}
	return net.Listen("unix", *addr)
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	// Sanity checks for arguments.
	if *postfixTransport == "" {
		log.Fatal("--transport must not be empty")
	}
	if *addr == "" {
		log.Fatal("--addr must not be empty")
	}

	// Create a TCP or UNIX socket listener.
	l, err := createListener()
	if err != nil {
		log.Fatalf("can't bind to %s: %v", *addr, err)
	}

	m := pot.NewBlacklistMap(pot.NewLimitMap(*maxInflight, pot.NewDNSLookupMap(*srvService, *postfixTransport)))
	loadBlacklist(m)

	srv := pot.NewServer(l, m)
	srv.EnableDebug = *debug

	// Setup a signal handler for graceful termination.
	sigCh := make(chan os.Signal)
	go func() {
		sig := <-sigCh
		if *debug {
			log.Printf("exiting: %s", sig)
		}
		srv.Close()
		os.Exit(0)
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	if *debug {
		log.Printf("listening on %s", *addr)
	}
	if err := srv.Serve(); err != nil {
		log.Fatal(err)
	}
}
