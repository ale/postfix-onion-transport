package pot

import (
	"errors"
	"fmt"
	"log"
	"net"
	"strings"
	"sync/atomic"
)

// Map implements a domain-based lookup table.
type Map interface {
	// Lookup a domain in the map. A note on error semantics: if
	// the lookup is successful and the domain is not found, the
	// result string will be empty and error will be nil. If there
	// is an error in the lookup, error will be non-nil.
	Lookup(string) (string, error)
}

// Interface wrapping net.LookupSRV so it can be stubbed out for
// testing.
type srvLookupFunc interface {
	LookupSRV(string, string, string) (string, []*net.SRV, error)
}

type defaultSRVLookupFunc struct{}

func (f defaultSRVLookupFunc) LookupSRV(service, proto, domain string) (string, []*net.SRV, error) {
	return net.LookupSRV(service, proto, domain)
}

// dnsLookupMap is a Map implementation that looks up _onion-mx._tcp
// SRV records on DNS.
type dnsLookupMap struct {
	lookupFn  srvLookupFunc
	service   string
	transport string
}

// NewDNSLookupMap returns a Map that looks up DNS SRV records and
// returns a result of the form transport:[SRV target].
func NewDNSLookupMap(service, transport string) Map {
	return &dnsLookupMap{
		service:   service,
		transport: transport,
		lookupFn:  &defaultSRVLookupFunc{},
	}
}

func (m *dnsLookupMap) Lookup(domain string) (string, error) {
	// Make the SRV request. We treat NXDOMAIN replies as
	// permanent errors (data does not exist), while other type of
	// lookup failures (i.e. inability to reach a DNS server)
	// cause temporary errors.
	_, addrs, err := m.lookupFn.LookupSRV(m.service, "tcp", domain)
	if err != nil {
		if dnsErr, ok := err.(net.Error); ok && (dnsErr.Temporary() || dnsErr.Timeout()) {
			// Log transport errors.
			log.Printf("DNS error: %v", err)
			return "", err
		}
		return "", nil
	}

	// Arbitrarily pick the first record returned that looks like
	// a hidden service.
	//
	// TODO: sort by priority?
	for _, addr := range addrs {
		if strings.HasSuffix(addr.Target, ".onion.") {
			return fmt.Sprintf("%s:[%s]:%d", m.transport, strings.TrimSuffix(addr.Target, "."), addr.Port), nil
		}
	}

	// No .onion targets were found.
	return "", nil
}

// limitMap prevents overload of an underlying lookupMap by
// controlling the number of in-flight requests.  This could happen
// with the dnsLookupMap, for instance, if the DNS resolvers become
// unresponsive. When the number of in-flight requests goes above a
// threshold, we apply backpressure to the client by returning a
// temporary error to Postfix.
type limitMap struct {
	inflight uint32
	max      uint32
	wrap     Map
}

var errPushback = errors.New("too many inflight requests")

// NewLimitMap wraps a Map with overload protection based on the
// number of inflight requests.
func NewLimitMap(maxInflight int, wrap Map) Map {
	return &limitMap{max: uint32(maxInflight), wrap: wrap}
}

func (m *limitMap) Lookup(domain string) (string, error) {
	defer atomic.AddUint32(&m.inflight, ^uint32(0))

	cur := atomic.AddUint32(&m.inflight, 1)
	if cur > m.max {
		return "", errPushback
	}
	return m.wrap.Lookup(domain)
}

// BlacklistMap wraps a Map by checking first the requested domain
// against a blacklist. If the domain appears in the blacklist, no
// further lookups are made and a permanent error is returned.
type BlacklistMap struct {
	bl   map[string]struct{}
	wrap Map
}

// NewBlacklistMap creates a BlacklistMap wrapping the given Map.
func NewBlacklistMap(wrap Map) *BlacklistMap {
	return &BlacklistMap{
		bl:   make(map[string]struct{}),
		wrap: wrap,
	}
}

// Add a domain to the blacklist.
func (m *BlacklistMap) Add(domain string) {
	m.bl[domain] = struct{}{}
}

func (m *BlacklistMap) Lookup(domain string) (string, error) {
	if _, ok := m.bl[domain]; ok {
		return "", nil
	}
	return m.wrap.Lookup(domain)
}
