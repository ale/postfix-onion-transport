package pot

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"net/textproto"
	"os"
	"strconv"
	"testing"
)

// A test Map that is just a plain map.
type testLookupMap map[string]string

func (m testLookupMap) Lookup(s string) (string, error) {
	return m[s], nil
}

// A test Map that can inject errors.
type testLookupMapWithErrors struct {
	m    map[string]string
	errs map[string]struct{}
}

func newTestLookupMapWithErrors() *testLookupMapWithErrors {
	return &testLookupMapWithErrors{
		m:    make(map[string]string),
		errs: make(map[string]struct{}),
	}
}

func (m *testLookupMapWithErrors) Lookup(s string) (string, error) {
	if _, ok := m.errs[s]; ok {
		return "", errors.New("an error occurred")
	}
	return m.m[s], nil
}

// Test client that speaks the tcp_table(5) protocol.
type testClient struct {
	conn net.Conn
	r    *textproto.Reader
	t    testing.TB
}

func newTestClient(t testing.TB, proto, addr string) *testClient {
	conn, err := net.Dial(proto, addr)
	if err != nil {
		t.Fatal("Dial:", err)
	}
	r := textproto.NewReader(bufio.NewReader(conn))
	return &testClient{conn: conn, t: t, r: r}
}

func (c *testClient) Close() {
	c.conn.Close()
}

func (c *testClient) request(key string) (int, string) {
	_, err := fmt.Fprintf(c.conn, "get %s\n", encodeParam(key))
	if err != nil {
		c.t.Fatal("send:", err)
	}
	resp, err := c.r.ReadLine()
	if err != nil {
		c.t.Fatal("recv:", err)
	}
	if len(resp) < 4 {
		c.t.Fatalf("reply too short: %v", resp)
	}
	statusCode, err := strconv.Atoi(resp[:3])
	if err != nil {
		c.t.Fatalf("malformed status code: %v", resp)
	}
	statusMessage, err := decodeParam(resp[4:])
	if err != nil {
		c.t.Fatalf("badly encoded message: %v", resp)
	}
	return statusCode, statusMessage
}

// Test server, can run on TCP or UNIX sockets.
type testServer struct {
	l    net.Listener
	addr string
	srv  *Server
	stop chan bool
}

func newTestServer(t testing.TB, proto string, m Map) *testServer {
	var l net.Listener
	var err error
	var addr string
	if proto == "tcp" {
		l, err = net.Listen(proto, "127.0.0.1:0")
		if err != nil {
			t.Fatal("Listen:", err)
		}
		addr = l.Addr().String()
	} else {
		addr = ".test.sock"
		l, err = net.Listen(proto, addr)
		if err != nil {
			t.Fatal("Listen:", err)
		}
	}

	srv := NewServer(l, m)
	stopCh := make(chan bool)
	go func() {
		srv.Serve()
	}()
	go func() {
		<-stopCh
		srv.Close()
	}()

	return &testServer{
		addr: addr,
		l:    l,
		srv:  srv,
		stop: stopCh,
	}
}

func (s *testServer) Close() {
	close(s.stop)
}

func TestServer_Listen(t *testing.T) {
	// Just check that there are no errors.
	srv := newTestServer(t, "tcp", nil)
	srv.Close()
}

func TestServer_ListenUNIX(t *testing.T) {
	// Just check that there are no errors.
	srv := newTestServer(t, "unix", nil)
	srv.Close()
}

func TestServer_LookupOk(t *testing.T) {
	m := make(testLookupMap)
	m["example.com"] = "abcdef.onion."

	srv := newTestServer(t, "tcp", m)
	defer srv.Close()

	c := newTestClient(t, "tcp", srv.addr)
	defer c.Close()

	// Expect this one to fail.
	code, _ := c.request("test@foo.bar")
	if code != statusCodeError {
		t.Fatalf("expected status %d, got %d", statusCodeError, code)
	}

	// Successful request.
	var msg string
	code, msg = c.request("test@example.com")
	if code != statusCodeOk {
		t.Fatalf("expected status %d, got %d", statusCodeOk, code)
	}
	if msg != "abcdef.onion." {
		t.Fatalf("expected value %s, got %s", "abcdef.onion.", msg)
	}
}

func TestServer_LookupTemporaryError(t *testing.T) {
	m := newTestLookupMapWithErrors()
	m.m["example.com"] = "abcdef.onion."
	m.errs["foo.bar"] = struct{}{}

	srv := newTestServer(t, "tcp", m)
	defer srv.Close()

	c := newTestClient(t, "tcp", srv.addr)
	defer c.Close()

	// Expect this one to fail temporarily.
	code, _ := c.request("test@foo.bar")
	if code != statusCodeTempError {
		t.Fatalf("expected status %d, got %d", statusCodeTempError, code)
	}

	// Successful request.
	var msg string
	code, msg = c.request("test@example.com")
	if code != statusCodeOk {
		t.Fatalf("expected status %d, got %d", statusCodeOk, code)
	}
	if msg != "abcdef.onion." {
		t.Fatalf("expected value %s, got %s", "abcdef.onion.", msg)
	}
}

func TestServer_Encoding(t *testing.T) {
	key := "\x0a\x0d"
	m := make(testLookupMap)
	m[key] = "abcdef.onion."

	srv := newTestServer(t, "tcp", m)
	defer srv.Close()

	c := newTestClient(t, "tcp", srv.addr)
	defer c.Close()

	// Expect this one to fail.
	code, msg := c.request("test@" + key)
	if code != statusCodeOk {
		t.Fatalf("expected status %d, got %d", statusCodeOk, code)
	}
	if msg != "abcdef.onion." {
		t.Fatalf("expected value %s, got %s", "abcdef.onion.", msg)
	}
}

func TestEncodeParam(t *testing.T) {
	testData := []struct {
		param  string
		result string
	}{
		{"foo", "foo"},
		{"\x0a\x0d", "%0A%0D"},
		{"%", "%25"},
		{"hello world", "hello%20world"},
	}
	for _, d := range testData {
		result := encodeParam(d.param)
		if result != d.result {
			t.Errorf("encodeParam(%v) returned %v, expected %v", d.param, result, d.result)
			continue
		}
		back, err := decodeParam(result)
		if err != nil {
			t.Errorf("encodeParam(%v) returned %v which gave a decoding error: %v", d.param, result, err)
			continue
		}
		if back != d.param {
			t.Errorf("encodeParam(%v) returned %v which decoded back to a different string (%v)", d.param, result, back)
		}
	}
}

// Benchmark lookups with a fake in-memory map (1 concurrent
// connection).
func BenchmarkServer_LookupNoDNS_1(b *testing.B) {
	runBenchmarkServerLookupNoDNS(b, 1)
}

// Benchmark lookups with a fake in-memory map (10 concurrent
// connections).
func BenchmarkServer_LookupNoDNS_10(b *testing.B) {
	runBenchmarkServerLookupNoDNS(b, 10)
}

// Benchmark lookups with a fake in-memory map (100 concurrent
// connections).
func BenchmarkServer_LookupNoDNS_100(b *testing.B) {
	runBenchmarkServerLookupNoDNS(b, 100)
}

func runBenchmarkServerLookupNoDNS(b *testing.B, parallelism int) {
	m := make(testLookupMap)
	m["example.com"] = "abcdef.onion."
	key := "test@example.com"
	runBenchmarkServerLookup(b, parallelism, m, key)
}

func runBenchmarkServerLookup(b *testing.B, parallelism int, m Map, key string) {
	srv := newTestServer(b, "tcp", m)
	defer srv.Close()

	b.ResetTimer()
	b.SetParallelism(parallelism)
	b.RunParallel(func(pb *testing.PB) {
		c := newTestClient(b, "tcp", srv.addr)
		defer c.Close()

		for pb.Next() {
			c.request(key)
		}
	})
	b.StopTimer()
}

// Run a benchmark with real DNS requests. Make sure that you control
// the resolver (a local one is preferable), as it will receive quite
// a few requests.
//
// To run, set the DOMAIN environment variable to the domain you want
// to test, for instance:
//
//     env DOMAIN=example.com go test -tags netgo \
//         -bench=.*_LookupDNS_100 ./
//
func BenchmarkServer_LookupDNS_100(b *testing.B) {
	domain := os.Getenv("DOMAIN")
	if domain == "" {
		b.Skip("environment variable DOMAIN not defined")
	}
	m := NewDNSLookupMap("onion-mx", "smtptor")
	runBenchmarkServerLookup(b, 100, m, "test@"+domain)
}
