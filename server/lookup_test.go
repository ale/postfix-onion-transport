package pot

import (
	"fmt"
	"net"
	"sync"
	"testing"
)

type fakeDNS struct {
	m map[string][]*net.SRV
}

func newFakeDNS() *fakeDNS {
	return &fakeDNS{m: make(map[string][]*net.SRV)}
}

func (f *fakeDNS) add(domain, target string, port int) {
	f.m[domain] = append(f.m[domain], &net.SRV{
		Target:   target,
		Port:     uint16(port),
		Priority: 10,
		Weight:   10,
	})
}

func (f *fakeDNS) LookupSRV(service, proto, domain string) (string, []*net.SRV, error) {
	key := fmt.Sprintf("_%s._%s.%s", service, proto, domain)
	resp, ok := f.m[key]
	if !ok {
		return "", nil, &net.DNSError{
			Err:  "NXDOMAIN",
			Name: key,
		}
	}
	return "", resp, nil
}

// Test that the dnsLookupMap actually performs the right DNS requests.
func TestSRVLookupMap(t *testing.T) {
	dns := newFakeDNS()
	dns.add("_onion-mx._tcp.example.com", "abcdef.onion.", 25)
	dns.add("_onion-mx._tcp.other.com", "not.a.hidden.service.", 25)

	m := NewDNSLookupMap("onion-mx", "smtptor")
	m.(*dnsLookupMap).lookupFn = dns

	// Simulate a successful request, verifying expected output
	// format.
	expected := "smtptor:[abcdef.onion]:25"
	resp, err := m.Lookup("example.com")
	if err != nil {
		t.Fatal("Lookup(example.com):", err)
	}
	if resp != expected {
		t.Fatalf("Lookup(example.com) returned %s, expected %s", resp, expected)
	}

	// Simulate a failed request (result does not exist, no
	// transport failures).
	resp, err = m.Lookup("notfound.com")
	if err != nil {
		t.Fatal("Lookup(notfound.com):", err)
	}
	if resp != "" {
		t.Fatalf("Lookup(notfound.com) returned non-empty response %s", resp)
	}

	// Simulate a failed request (SRV record does not point to a
	// hidden service, no transport failures).
	resp, err = m.Lookup("other.com")
	if err != nil {
		t.Fatal("Lookup(other.com):", err)
	}
	if resp != "" {
		t.Fatalf("Lookup(other.com) returned non-empty response %s", resp)
	}
}

// A srvLookupFunc that fails with a temporary error.
type failDNS struct{}

func (f *failDNS) LookupSRV(service, proto, domain string) (string, []*net.SRV, error) {
	key := fmt.Sprintf("_%s._%s.%s", service, proto, domain)
	return "", nil, &net.DNSError{
		Err:         "temporary failure",
		Name:        key,
		IsTemporary: true,
	}
}

// Test that the dnsLookupMap returns a temporary error when we can't
// resolve targets for whatever reason.
func TestSRVLookupMap_TransportError(t *testing.T) {
	m := NewDNSLookupMap("onion-mx", "smtptor")
	m.(*dnsLookupMap).lookupFn = &failDNS{}

	_, err := m.Lookup("example.com")
	if err == nil {
		t.Fatal("Lookup(example.com) did not return an error")
	}
}

// Requests to this Map will hang forever until the 'ch' channel is
// closed. Use the WaitGroup to count blocked requests.
type sinkMap struct {
	ch chan bool
	wg sync.WaitGroup
}

func (s *sinkMap) Lookup(domain string) (string, error) {
	s.wg.Done()
	<-s.ch
	return "foo", nil
}

// Verify that backpressure kicks in when there are too many inflight
// requests.
func TestLimitMap(t *testing.T) {
	limit := 10
	stopCh := make(chan bool)
	defer close(stopCh)
	sink := &sinkMap{ch: stopCh}
	m := NewLimitMap(limit, sink)

	// Spawn 'limit' requests. These should all go to the underlying map.
	for i := 0; i < limit; i++ {
		sink.wg.Add(1)
		go func() {
			m.Lookup("example.com")
		}()
	}

	// Spawn another request and verify that we get errPushback.
	// The WaitGroup is used to ensure this request comes after
	// the previous 10.
	sink.wg.Wait()
	_, err := m.Lookup("example.com")
	if err != errPushback {
		t.Errorf("unexpected error from Lookup: %v", err)
	}
}

// Test the BlacklistMap implementation.
func TestBlacklistMap(t *testing.T) {
	dns := newFakeDNS()
	dns.add("_onion-mx._tcp.example.com", "abcdef.onion.", 25)
	dns.add("_onion-mx._tcp.foo.bar", "bcdefg.onion.", 25)

	m := NewDNSLookupMap("onion-mx", "smtptor")
	m.(*dnsLookupMap).lookupFn = dns

	bl := NewBlacklistMap(m)
	bl.Add("foo.bar")

	// Request for example.com should succeed.
	resp, err := bl.Lookup("example.com")
	if err != nil {
		t.Fatal("Lookup(example.com):", err)
	}
	if resp == "" {
		t.Fatal("Lookup(example.com) not found")
	}

	// Request for foo.bar should be blacklisted.
	resp, err = bl.Lookup("foo.bar")
	if err != nil {
		t.Fatal("Lookup(foo.bar):", err)
	}
	if resp != "" {
		t.Fatal("Lookup(foo.bar) not nil, blacklist failed")
	}
}
