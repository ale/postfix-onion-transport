package pot

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"net/textproto"
	"net/url"
	"strconv"
	"strings"
)

const (
	statusCodeOk        = 200
	statusCodeTempError = 400
	statusCodeError     = 500
)

// A tcp_table response.
type status struct {
	code    int
	message string
}

func (s *status) writeTo(w io.Writer) error {
	_, err := fmt.Fprintf(w, "%d %s\n", s.code, encodeParam(s.message))
	return err
}

// Convenience function to generate a permanent error.
func statusError(s string) *status {
	return &status{code: statusCodeError, message: s}
}

// Convenience function to generate a temporary error.
func statusTempError(s string) *status {
	return &status{code: statusCodeTempError, message: s}
}

// Convenience function to generate a successful result.
func statusOk(s string) *status {
	return &status{code: statusCodeOk, message: s}
}

// Server implements the protocol described in Postfix's tcp_table(5).
type Server struct {
	l    net.Listener
	lmap Map

	// EnableDebug enables debug logging of every request.
	EnableDebug bool
}

// NewServer builds a new Server using the given Listener and
// Map.
func NewServer(l net.Listener, lmap Map) *Server {
	return &Server{l: l, lmap: lmap}
}

// Close the listener and free all associated resources.
func (s *Server) Close() error {
	return s.l.Close()
}

// Serve requests forever (or until a fatal error occurs).
func (s *Server) Serve() error {
	for {
		conn, err := s.l.Accept()
		if err != nil {
			return err
		}
		go s.handleConnection(conn)
	}
}

// handleConnection handles multiple requests on a single incoming
// connection, until it is closed by the client.
func (s *Server) handleConnection(c net.Conn) {
	defer c.Close()

	r := textproto.NewReader(bufio.NewReader(c))
	for {
		line, err := r.ReadLine()
		if err == io.EOF {
			return
		} else if err != nil {
			log.Printf("%s: %v", c.RemoteAddr().String(), err)
			return
		}
		resp := s.handleRequest(line)
		if s.EnableDebug {
			log.Printf("%s -> %d %s", line, resp.code, resp.message)
		}
		if err := resp.writeTo(c); err != nil {
			log.Printf("%s: write error: %v", c.RemoteAddr().String(), err)
			return
		}
	}
}

// handleRequest deals with a single incoming request, and returns a status.
func (s *Server) handleRequest(req string) *status {
	if !strings.HasPrefix(req, "get ") {
		return statusError("unknown request")
	}
	key, err := decodeParam(req[4:])
	if err != nil {
		return statusError(fmt.Sprintf("unable to decode request: %v", err))
	}

	// Extract the domain name from the request key.
	atIdx := strings.Index(key, "@")
	if atIdx < 0 {
		return statusError(fmt.Sprintf("request key is not an email address"))
	}
	domain := key[atIdx+1:]

	// Perform the lookup and return the encoded response.
	resp, err := s.lmap.Lookup(domain)
	if err != nil {
		return statusTempError(err.Error())
	}
	if resp == "" {
		return statusError("not found")
	}
	return statusOk(resp)
}

// Postfix encodes requests using the %XX hex-encoded syntax also used
// by RFC 3986, but the selection of encoded characters differs, so we
// have to reimplement some stuff from net/url.
func toHex(c byte) byte {
	return "0123456789ABCDEF"[c]
}

func shouldEscape(c byte) bool {
	return c == '%' || c == ' ' || c == '\t' || c == '\n' || !strconv.IsPrint(rune(c))
}

func encodeParam(s string) string {
	// Iterate over bytes, not runes (encoding-neutral).
	numEsc := 0
	for i := 0; i < len(s); i++ {
		if shouldEscape(s[i]) {
			numEsc++
		}
	}

	// Optimize best-case.
	if numEsc == 0 {
		return s
	}

	out := make([]byte, len(s)+numEsc*2)
	j := 0
	for i := 0; i < len(s); i++ {
		switch c := s[i]; {
		case shouldEscape(c):
			out[j] = '%'
			out[j+1] = toHex(c >> 4)
			out[j+2] = toHex(c & 0x0f)
			j += 3
		default:
			out[j] = c
			j++
		}
	}
	return string(out)
}

func decodeParam(s string) (string, error) {
	return url.QueryUnescape(s)
}
